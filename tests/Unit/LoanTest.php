<?php

namespace Tests\Unit;

use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use App\User;

class LoanTest extends TestCase
{
    use DatabaseTransactions;
    /**
     * A basic unit test example.
     *
     * @return void
     */

    public function testCreateLoanWithoutLogin()
    {
        $data = [
            'amount' => 1000,
            'duration' => 12,
            'repay_frequency' => 1
        ];

        $response = $this->json('POST', '/api/loans',$data);
        $response->assertStatus(401);
        $response->assertJson(['message' => "Unauthenticated."]);
    }

    public function testCreateLoan()
    {
        $this->beginDatabaseTransaction();
        $user = factory(User::class)->create();
        $response = $this->withHeaders([
            'X-Requested-With' => 'XMLHttpRequest'
        ])->actingAs($user, 'api')
            ->post('/api/loans',[
                'amount' => 1000,
                'duration' => 12,
                'repay_frequency' => 1
            ]);
        $response->assertStatus(201);
    }
}
