<?php

namespace Tests\Unit;

use App\Loan;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;
use App\User;

class RepaymentTest extends TestCase
{
    use DatabaseTransactions;
    /**
     * A basic unit test example.
     *
     * @return void
     */

    public function testMakeRepaymentOnUnapprovedLoan()
    {
        $this->beginDatabaseTransaction();
        $user = factory(User::class)->create();
        $jsonContent = $this->withHeaders([
            'X-Requested-With' => 'XMLHttpRequest'
        ])->actingAs($user, 'api')
            ->post('/api/loans',[
                'amount' => 1000,
                'duration' => 12,
                'repay_frequency' => 1
            ])->decodeResponseJson();
        $response = $this->actingAs($user, 'api')
            ->post('/api/repayments', [
                'loan_id' => $jsonContent['id']
            ]);
        $response->assertJson([
            'error' => true,
            'message' => 'Could not make repayment on this loan.'
        ]);

    }

    public function testMakeRepaymentOnApprovedLoan()
    {
        $this->beginDatabaseTransaction();
        $user = factory(User::class)->create();
        $jsonContent = $this->withHeaders([
            'X-Requested-With' => 'XMLHttpRequest'
        ])->actingAs($user, 'api')
            ->post('/api/loans',[
                'amount' => 1000,
                'duration' => 12,
                'repay_frequency' => 1
            ])->decodeResponseJson();
        $loan = Loan::find($jsonContent['id']);
        $loan->status = Loan::STATUS_APPROVED;
        $loan->save();
        $response = $this->actingAs($user, 'api')
            ->post('/api/repayments', [
                'loan_id' => $jsonContent['id'],
                'amount' => 1000
            ]);
        $response->assertStatus(201);
    }
}
