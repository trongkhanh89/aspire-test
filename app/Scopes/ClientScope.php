<?php
/**
 * Created by PhpStorm.
 * User: trong
 * Date: 06-Jul-18
 * Time: 10:06 AM
 */

namespace App\Scopes;


use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Scope;

class ClientScope implements Scope
{
    public function apply(Builder $builder, Model $model)
    {
        if(!app()->runningInConsole()) {
            $builder->where($model->getTable() .'.user_id', auth()->user()->id);
        }
    }
}