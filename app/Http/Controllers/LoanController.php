<?php
/**
 * Created by PhpStorm.
 * User: khanhhuynh
 * Date: 2019-08-25
 * Time: 11:55
 */

namespace App\Http\Controllers;


use App\Http\Requests\Loan\StoreRequest;
use App\Http\Requests\Loan\UpdateRequest;
use App\Loan;

class LoanController extends Controller
{
    public function store(StoreRequest $request)
    {
        $result = Loan::create([
            'amount' => $request->amount,
            'duration' => $request->duration,
            'repay_frequency' => $request->repay_frequency
        ]);

        return $result;
    }

}