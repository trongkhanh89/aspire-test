<?php
/**
 * Created by PhpStorm.
 * User: khanhhuynh
 * Date: 2019-08-25
 * Time: 16:48
 */

namespace App\Http\Controllers;


use App\Http\Requests\Repayment\StoreRequest;
use App\Loan;

class RepaymentController extends Controller
{

    public function store(StoreRequest $request)
    {
        $loan = Loan::findOrFail($request->loan_id);
        if($loan->isPaid()) {
            return response()->json([
                'message' => 'This loan has been paid.'
            ]);
        }
        if($loan->isRepayable()) {
            $repaid = $loan->repayments()->create([
                'amount' => $request->amount
            ]);
            return $repaid;
        } else {
            return response()->json([
                'error' => true,
                'message' => 'Could not make repayment on this loan.'
            ]);
        }
    }

}