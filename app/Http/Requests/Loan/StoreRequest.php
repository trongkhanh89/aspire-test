<?php
/**
 * Created by PhpStorm.
 * User: khanhhuynh
 * Date: 2019-08-25
 * Time: 12:22
 */

namespace App\Http\Requests\Loan;


use Illuminate\Foundation\Http\FormRequest;

class StoreRequest extends FormRequest
{

    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'amount' => 'required|numeric',
            'duration' => 'required|numeric',
            'repay_frequency' => 'required|numeric'
        ];
    }

}