<?php
/**
 * Created by PhpStorm.
 * User: khanhhuynh
 * Date: 2019-08-25
 * Time: 16:49
 */

namespace App\Http\Requests\Repayment;


use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class StoreRequest extends FormRequest
{

    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'loan_id' => [
                'required',
                Rule::exists('loans', 'id')->where(function($query) {
                    $query->where('user_id', auth()->user()->id);
                })
            ]
        ];
    }

}