<?php

namespace App;

use App\Traits\ClientTrait;
use Illuminate\Database\Eloquent\Model;

class Repayment extends Model
{
    use ClientTrait;

    protected $table = 'repayments';

    protected $fillable = ['amount'];

    public function loan()
    {
        return $this->belongsTo(Loan::class);
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
