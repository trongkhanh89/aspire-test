<?php

namespace App;

use App\Traits\ClientTrait;
use Illuminate\Database\Eloquent\Model;

class Loan extends Model
{
    use ClientTrait  {
        boot as clientBoot;
    }

    const STATUS_SUBMITTED = 0;

    const STATUS_APPROVED = 1;

    protected $table = 'loans';
    
    protected $fillable = ['amount', 'duration', 'repay_frequency', 'arrangement_fee', 'interest_rate'];

    public static function boot()
    {
        static::creating(function(self $model) {
            $model->interest_rate = config('aspire.interest_rate');
            $model->arrangement_fee = config('aspire.arrangement_fee');
        });
        self::clientBoot();
        parent::boot();
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function repayments()
    {
        return $this->hasMany(Repayment::class);
    }

    public function isApproved()
    {
        return $this->status == self::STATUS_APPROVED;
    }

    public function getTotalAmountAttribute()
    {
        return $this->amount + ($this->interest_rate * $this->duration) + $this->arrangement_fee;
    }

    public function getRemainingAmountAttribute()
    {
        return max($this->total_amount - $this->repayments()->sum('amount'), 0);
    }

    public function isPaid()
    {
        return $this->remaining_amount == 0;
    }

    public function isRepayable()
    {
        return $this->isApproved() && $this->remaining_amount > 0;
    }

}
