<?php
/**
 * Created by PhpStorm.
 * User: khanhhuynh
 * Date: 2019-08-25
 * Time: 12:30
 */

namespace App\Traits;
use App\Scopes\ClientScope;
use App\User;

trait ClientTrait
{

    public static function boot()
    {
        parent::boot();
        static::addGlobalScope(new ClientScope());
        static::creating(function($model) {
            $model->user_id = auth()->user()->id;
            return $model;
        });

    }

    public function client()
    {
        return $this->belongsTo(User::class, 'user_id');
    }

}